---
title: 'Content'
description: ''
---

These examples show basic usage of what [Bootstrap content](http://getbootstrap.com/docs/4.1/content/reboot/) looks like in FSO Bootstrap, including any extra color options and varieties of the default controls. For full documentation of these css classes and utilities, see bootstrap's content documentation linked above.
