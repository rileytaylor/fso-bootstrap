---
title: 'Code'
description: 'Documentation and examples for displaying inline and multiline blocks of code with Bootstrap.'
buttons: 
- label: Bootstrap Docs
  icon: fas fa-book   
  type: info   
  link: 'http://getbootstrap.com/docs/4.1/content/code/'
---

## Inline code

Wrap inline snippets of code with `code`. Be sure to escape HTML angle brackets.

<Example>
  <p>For example, <code>&lt;section&gt;</code> should be wrapped as inline.</p>
</Example>

::: tip FYI
The above example renders the &lt;code&gt; element using the styling of this documentation site, not as it is in fso-bootstrap, so you will likely see some different rendering in action.
:::

## Code blocks

Use `pre`s for multiple lines of code. Once again, be sure to escape any angle brackets in the code for proper rendering. You may optionally add the `.pre-scrollable` class, which will set a max-height of 340px and provide a y-axis scrollbar.

<Example>
  <pre><code>&lt;p&gt;Sample text here...&lt;/p&gt;
  &lt;p&gt;And another line of sample text here...&lt;/p&gt;
  </code></pre>
</Example>

## Variables

For indicating variables use the `var` tag.

<Example>
  <p><var>y</var> = <var>m</var><var>x</var> + <var>b</var></p>
</Example>

## User input

Use the `kbd` to indicate input that is typically entered via keyboard.

<Example>
  <p>To switch directories, type <kbd>cd</kbd> followed by the name of the directory.<br></p>
  <p>To edit settings, press <kbd>ctrl</kbd> + <kbd>,</kbd></p>
</Example>

## Sample output

For indicating sample output from a program use the `samp` tag.

<Example>
  <p><samp>This text is meant to be treated as sample output from a computer program.</samp></p>
</Example>
