---
home: true
heroImage: /hero.png
actionText: Get Started
actionLink: /usage/
features:
- title: Go Fast
  details: Easy to use css styling lets you design apps quickly. It is also extremely light-weight, coming in at less than >200kb, so your apps will literally go faster too.
  image: images/gofast.svg
- title: Bear Down
  details: Red and Blue coloring will have comrades humming Bear Down as they do their part for the motherland.
  image: images/fist.svg
- title: Design Conscious
  details: UA branding is intended for informational pages, so we tone it down to fit better with expectations for modern web applications. When necessary, certain UA Bootstrap features can be implemented on the fly with additional classes.
  image: images/award.svg
footer: MIT Licensed | Copyright © 2017-present Financial Services Office
---
