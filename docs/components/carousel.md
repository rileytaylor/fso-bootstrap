---
title: 'Carousel'
description: 'A slideshow component for cycling through elements—images or slides of text—like a carousel.'
buttons: 
- label: Bootstrap Docs
  icon: fas fa-book   
  type: info   
  link: 'http://getbootstrap.com/docs/4.1/components/carousel/'
---

See [Bootstrap's carousel documentation](http://getbootstrap.com/docs/4.1/components/carousel/) to learn more.
