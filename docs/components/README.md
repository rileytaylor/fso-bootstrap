---
title: 'Components'
description: ''
---

These examples show basic usage of what [Bootstrap components](http://getbootstrap.com/docs/4.1/components/alerts/) look like in FSO Bootstrap, including any extra color options and varieties of the default controls. Most components include the bulk of bootstrap's normal documentation for ease-of-reading and for testing changes. Be sure to check the full documentation of these components. Each page includes a link to bootstraps relavent documentation at the top of the page.

## New Components

FSO Bootstrap includes several components not included by Bootstrap. As such, their complete documentation is found here on this site. 

New components include:

- [Font Awesome Icons\*](./font-awesome-icons.html)
- [UA Branding Icons\*](./ua-brand-icons.html)

\* Indicates an opt-in component which must be imported seperately. Each component's documentation page will explain how to do so.
