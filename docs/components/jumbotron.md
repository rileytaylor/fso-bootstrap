---
title: 'Jumbotron'
description: 'Lightweight, flexible component for showcasing hero unit style content.'
buttons: 
- label: Bootstrap Docs
  icon: fas fa-book   
  type: info   
  link: 'http://getbootstrap.com/docs/4.1/components/jumbotron/'
---

A lightweight, flexible component that can optionally extend the entire viewport to showcase key marketing messages on your site.

<example>
    <div class="jumbotron">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>
</example>

To make the jumbotron full width, and without rounded corners, add the `.jumbotron-fluid` modifier class and add a `.container` or `.container-fluid` within.

<example>
    <div class="jumbotron jumbotron-fluid">
        <div class="container">
            <h1 class="display-4">Fluid jumbotron</h1>
            <p class="lead">This is a modified jumbotron that occupies the entire horizontal space of its parent.</p>
        </div>
    </div>
</example>

## Variations

When needed, use the dark variant. Or, when a more flashy jumbotron is needed, use either the Blue or Red options to showcase UA colors.

### Dark

<example>
    <div class="jumbotron jumbotron-dark">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>
</example>

### Blue

<example>
    <div class="jumbotron jumbotron-blue">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn btn-accent btn-lg" href="#" role="button">Learn more</a>
    </div>
</example>

### Red

<example>
    <div class="jumbotron jumbotron-red">
        <h1 class="display-4">Hello, world!</h1>
        <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>
        <hr class="my-4">
        <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>
        <a class="btn btn-primary btn-lg" href="#" role="button">Learn more</a>
    </div>
</example>
