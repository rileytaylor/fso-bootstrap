---
title: 'Scrollspy'
description: 'Automatically update Bootstrap navigation or list group components based on scroll position to indicate which link is currently active in the viewport.'
buttons: 
- label: Bootstrap Docs
  icon: fas fa-book   
  type: info   
  link: 'http://getbootstrap.com/docs/4.1/components/scrollspy/'
---

See [bootstraps documentation](http://getbootstrap.com/docs/4.1/components/scrollspy/) to learn about Scrollspy.
