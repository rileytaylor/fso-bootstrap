---
title: 'Badges'
description: 'Documentation and examples for badges, our small count and labeling component.'
buttons: 
- label: Bootstrap Docs
  icon: fas fa-book   
  type: info   
  link: 'http://getbootstrap.com/docs/4.1/components/badge/'
---

Badges scale to match the size of the immediate parent element by using relative font sizing and `em` units.

<example>
    <h1>Example heading <span class="badge badge-secondary">New</span></h1>
    <h2>Example heading <span class="badge badge-secondary">New</span></h2>
    <h3>Example heading <span class="badge badge-secondary">New</span></h3>
    <h4>Example heading <span class="badge badge-secondary">New</span></h4>
    <h5>Example heading <span class="badge badge-secondary">New</span></h5>
    <h6>Example heading <span class="badge badge-secondary">New</span></h6>
</example>

Badges can be used as part of links or buttons to provide a counter.

<example>
    <button type="button" class="btn btn-primary">
        Notifications <span class="badge badge-light">4</span>
    </button>
</example>

## Contextual Variations

<example>
    <span class="badge badge-primary">Primary</span>
    <span class="badge badge-secondary">Secondary</span>
    <span class="badge badge-success">Success</span>
    <span class="badge badge-danger">Danger</span>
    <span class="badge badge-warning">Warning</span>
    <span class="badge badge-info">Info</span>
    <span class="badge badge-light">Light</span>
    <span class="badge badge-dark">Dark</span>
</example>

## Pill

<example>
    <span class="badge badge-pill badge-primary">Primary</span>
    <span class="badge badge-pill badge-secondary">Secondary</span>
    <span class="badge badge-pill badge-success">Success</span>
    <span class="badge badge-pill badge-danger">Danger</span>
    <span class="badge badge-pill badge-warning">Warning</span>
    <span class="badge badge-pill badge-info">Info</span>
    <span class="badge badge-pill badge-light">Light</span>
    <span class="badge badge-pill badge-dark">Dark</span>
</example>


